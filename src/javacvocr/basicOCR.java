package javacvocr;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.*;
import com.googlecode.javacv.OpenCVFrameGrabber;
import static com.googlecode.javacv.cpp.opencv_core.cvFlip;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;

import static com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvFlip;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2RGB;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JFrame;
import com.googlecode.javacv.*;
import com.googlecode.javacv.cpp.*;
import com.googlecode.javacv.cpp.opencv_core.CvArr;
import com.googlecode.javacv.cpp.opencv_core.CvMat;
import com.googlecode.javacv.cpp.opencv_ml.CvKNearest;

import com.googlecode.javacpp.Loader;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import java.awt.image.BufferedImage;

public class basicOCR extends source {

	private static int classes = 10;
	private static int train_samples = 49;
	private static String file_path = "";
	private static int size = 40;
	private static CvMat trainClasses = new CvMat();
	private static CvMat trainData = new CvMat();
	private static int K = 8;
	private static CvKNearest knn;

    public basicOCR() {

    	trainData = cvCreateMat(train_samples*classes, size*size, CV_32FC1);
    	trainClasses = cvCreateMat(train_samples*classes, 1, CV_32FC1);
    	System.out.println(" ---------------------------------------------------------------\n");
    	System.out.println("|\tClass\t|\tPrecision\t|\tAccuracy\t|\n");
    	System.out.println(" ---------------------------------------------------------------\n");
    }

	public void getData()
	{

		source a = new source();
		IplImage src_image;
		IplImage prs_image;
		CvMat row = new CvMat();
		CvMat data = new CvMat();

		String file = new String();
		int i,j;
		for(i =0; i<classes ; i++){
			for( j = 0; j< train_samples ; j++){

				//Load file
				if(j<10)					
					file = String.format("%s%d\\%d0%d.pbm",file_path , i, i , j);
				else					
					file = String.format("%s%d\\%d%d.pbm",file_path , i, i , j);
				src_image = cvLoadImage(file,0);
				if(src_image == null){
					System.out.format("Error: Cant load image %s\n", file);					
				}
				//process file				
				prs_image = preProcessing(src_image, size , size);				
				//Set class label
				cvGetRow(trainClasses, row, i*train_samples + j);
				cvSet(row, cvRealScalar(i));
				//Set data
				cvGetRow(trainData, row, i*train_samples + j);

				IplImage img = cvCreateImage( cvSize( size, size ), IPL_DEPTH_32F, 1 );
				//convert 8 bits image to 32 float image
				cvConvertScale(prs_image, img, 0.0039215, 0);

				cvGetSubRect(img, data, cvRect(0,0, size,size));

				CvMat row_header = new CvMat();
				CvMat row1 = new CvMat();
				//convert data matrix sizexsize to vecor
				row1 = cvReshape( data, row_header, 0, 1 );
				cvCopy(row1, row, null);
			}
		}

		train();

	}

	private void train() {	
		System.out.println("training..");
		knn= new CvKNearest( trainData, trainClasses, null, false, K );
		System.out.println("training complete !");
	}


	public void test() {

		IplImage src_image;
		IplImage prs_image;
		CvMat row,data;
		String file = new String();
		int i,j;
		int error=0;
		int testCount=0;
		for(i =0; i<classes; i++){
			for( j = 50; j< 50+train_samples; j++){

				file = String.format("%s%d\\%d%d.pbm",file_path , i, i , j);
				src_image = cvLoadImage(file,0);
				if(src_image == null){
					System.out.format("Error: Cant load image %s\n", file);					
				}
				//process file
				prs_image = preProcessing(src_image, size, size);
				float r=classify(prs_image, false);
				if((int)r!=i)
					error++;

				testCount++;
			}
		}
		float totalerror=100*(float)error/(float)testCount;
		System.out.format("System Error: %.2f%%\n", totalerror);	


	}

	public float classify(IplImage img, Boolean showResult) {

		IplImage prs_image;
		CvMat data = new CvMat();
		CvMat nearest=cvCreateMat(1,K,CV_32FC1); 		
		float result;
		//process file
		prs_image = preProcessing(img, size, size);

		//Set data
		IplImage img32 = cvCreateImage( cvSize( size, size ), IPL_DEPTH_32F, 1 );
		cvConvertScale(prs_image, img32, 0.0039215, 0);
		cvGetSubRect(img32, data, cvRect(0,0, size,size));
		CvMat row_header = new CvMat();
		CvMat row1 = new CvMat();
		row1 = cvReshape( data, row_header, 0, 1 );

		result = knn.find_nearest(row1, K, null, null, nearest, null);

		int accuracy=0;
		for(int i=0;i<K;i++){			
			if( nearest.get(i) == result)
				accuracy++;
		}
		float pre=100*((float)accuracy/(float)K );
		if(showResult == true){
			System.out.format("|\t%.0f\t| \t%.2f%% \t| \t%d of %d \t| \n",result,pre,accuracy,K);
			System.out.format(" ---------------------------------------------------------------\n");
		}

		return result;


	}


}
