package javacvocr; 

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.*;
import com.googlecode.javacv.OpenCVFrameGrabber;
import static com.googlecode.javacv.cpp.opencv_core.cvFlip;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;

import static com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvFlip;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2RGB;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JFrame;
import com.googlecode.javacv.*;
import com.googlecode.javacv.cpp.*;

import com.googlecode.javacpp.Loader;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import java.awt.image.BufferedImage;

public class source{
	
	
	private static String file_path = "";
	static CanvasFrame canvas1= new CanvasFrame("Image", 1);
	
    
	
 public static void main(String[] args) {	 
	 
	 int class_num= 3;
	 int img_num= 98;
	 String file = String.format("%s%d\\%d%d.pbm",file_path , class_num , class_num , img_num);
	 
	 IplImage image= cvLoadImage("nums.jpg",0);


	 canvas1.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
      
     
       
	   CvSeq contours= findContours(image);
       IplImage resImage = drawContours(image, contours);
       canvas1.showImage(resImage);
       System.out.println("done..!");
	
    }
 
 private IplImage findText(IplImage origImage, CvSeq contours, int numToFind) {
	 
	  basicOCR a = new basicOCR();
      a.getData();
      a.test(); 
      a.classify(origImage, true); 
      
	 
	 
	return origImage;	 
 }
 
 
 private int[] findX(IplImage imgSrc,int min, int max){
	 int[] result= new int[2];
	 int i;
	 boolean minFound = false;
	 CvMat data = new CvMat();
	 CvScalar maxVal=cvRealScalar(imgSrc.height() * 255);
	 CvScalar val=cvRealScalar(0);
	 //For each col sum, if sum < width*255 then we find the min
	 //then continue to end to search the max, if sum< width*255 then is new max
	 for (i=0; i< imgSrc.width(); i++){
		 cvGetCol(imgSrc, data, i);
		 val= cvSum(data);
		 if(val.val(0) < maxVal.val(0)){
			 max= i;
			 if(!minFound){
				 min= i;
				 minFound= true;
			 }
		 }
	 }
	
	 result[0]= min;
	 result[1]= max;
	 return result;
 }

 private int[] findY(IplImage imgSrc,int min, int max){
	 int[] result= new int[2];
	 int i;
	 boolean minFound = false;
	 CvMat data = new CvMat();;
	 CvScalar maxVal=cvRealScalar(imgSrc.width() * 255);
	 CvScalar val=cvRealScalar(0);
	 //For each col sum, if sum < width*255 then we find the min
	 //then continue to end to search the max, if sum< width*255 then is new max
	 for (i=0; i< imgSrc.height(); i++){
		 cvGetRow(imgSrc, data, i);
		 val= cvSum(data);
		 if(val.val(0) < maxVal.val(0)){
			 max=i;
			 if(!minFound){
				 min= i;
				 minFound= true;
			 }
		 }
	 }	 
	 
	 result[0]= min;
	 result[1]= max;
	 return result;	 
 }
 
 private CvRect findBB(IplImage imgSrc){
	 CvRect aux;
	 int xmin, xmax, ymin, ymax;
	 int[] result= new int[2];
	 xmin=xmax=ymin=ymax=0;

	 result = findX(imgSrc, xmin, xmax);
	 xmin= result[0];
	 xmax= result[1];

	 result = findY(imgSrc, ymin, ymax);
	 ymin= result[0];
	 ymax= result[1];

	 aux=cvRect(xmin, ymin, xmax-xmin + 1, ymax-ymin + 1);


	 return aux;

 }

 public IplImage preProcessing(IplImage imgSrc,int new_width, int new_height){
	 IplImage result;
	 IplImage scaledResult;

	 CvMat data = new CvMat();
	 CvMat dataA = new CvMat();
	 CvRect bb;//bounding box
	 CvRect bba;//bounding box maintain aspect ratio

	 //Find bounding box
	 bb=findBB(imgSrc);

	 //Get bounding box data and no with aspect ratio, the x and y can be corrupted
	 cvGetSubRect(imgSrc, data, cvRect(bb.x(), bb.y(), bb.width(), bb.height()));

	 //Create image with this data with width and height with aspect ratio 1
	 //then we get highest size betwen width and height of our bounding box
	 int size=(bb.width()>bb.height())?bb.width():bb.height();
	 result=cvCreateImage( cvSize( size, size ), 8, 1 );
	 cvSet(result,CV_RGB(255,255,255),null);

	 //Copy de data in center of image
	 int x=(int)Math.floor((float)(size-bb.width())/2.0f);
	 int y=(int)Math.floor((float)(size-bb.height())/2.0f);

	 cvGetSubRect(result, dataA, cvRect(x,y,bb.width(), bb.height()));
	 cvCopy(data, dataA, null);

	 //Scale result
	 scaledResult=cvCreateImage( cvSize( new_width, new_height ), 8, 1 );
	 cvResize(result, scaledResult, CV_INTER_NN);

	 //Return processed data
	 return scaledResult;

 } 
 
 
 private static CvSeq findContours(IplImage orig_image) {
	 basicOCR a = new basicOCR();
     a.getData();
	 IplImage resultImage = cvCloneImage(orig_image);
	 IplImage image = cvCloneImage(orig_image);
	 
	    CvMemStorage mem = CvMemStorage.create();
	   
	    CvSeq imageContours = new CvSeq();
	    CvSeq ptr = new CvSeq();
	    cvThreshold(orig_image, image, 230, 255, CV_THRESH_BINARY_INV);
	    cvFindContours(image, mem, imageContours, Loader.sizeof(CvContour.class) , CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
	    
	    return imageContours;
 }

 private static IplImage drawContours(IplImage orig_image, CvSeq contours) {
	 IplImage resultImage = cvCloneImage(orig_image);  
	 CvRect boundbox;
	 CvSeq ptr = new CvSeq();
	 int i= 1;
	 for (ptr = contours; ptr != null; ptr = ptr.h_next()) {
		 boundbox = cvBoundingRect(ptr, 0);

		 cvRectangle( resultImage , cvPoint( boundbox.x(), boundbox.y() ), 
				 cvPoint( boundbox.x() + boundbox.width(), boundbox.y() + boundbox.height()),
				 cvScalar( 0+i, 0, 0, 0 ), 1, 0, 0 );
		 i=i+20;


		 
		 cvSetImageROI(orig_image,cvRect( boundbox.x(), boundbox.y() ,  boundbox.width(), boundbox.height()));

		 IplImage img2 = cvCreateImage(cvGetSize(orig_image),
				 orig_image.depth(),
				 orig_image.nChannels());

		 cvCopy(orig_image, img2, null);

		 canvas1.showImage(img2);	    

		 cvResetImageROI(orig_image);	

	 }

	 return resultImage;

 }

}
 
